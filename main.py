import csv
from auto import tyvave


# Lecture Fichier

with open('auto.csv', 'r') as incsv:
    reader = csv.DictReader(incsv, delimiter='|')
    with open('voiture.csv','w') as csvfile:
        writer = csv.writer(csvfile, delimiter=';')
        writer.writerow(['address','name','firstname','immat','date_immat','vin','marque','denomination','couleur','carrosserie','categorie','cylindree','energy','places','poids','puissance','type','variante','version'])
        for row in reader:
            a=row['address']
            b=row['name']
            c=row['firstname']
            d=row['immat']
            e=row['date_immat']
            f=row['vin']
            g=row['marque']
            h=row['denomination']
            i=row['couleur']
            j=row['carrosserie']
            k=row['categorie']
            l=row['cylindree']
            m=row['energy']
            n=row['places']
            o=row['poids']
            p=row['puissance']
            q=tyvave(row['type_variante_version'])[0]
            r=tyvave(row['type_variante_version'])[1]
            s=tyvave(row['type_variante_version'])[2]
            writer.writerow([a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s])
        
